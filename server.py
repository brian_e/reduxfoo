#! /usr/bin/env python2

import SimpleHTTPServer
import SocketServer
import time

PORT = 8000


class Handler(SimpleHTTPServer.SimpleHTTPRequestHandler, object):
    def do_GET(self):
        time.sleep(0.050)
        return super(Handler, self).do_GET()


def main():
    httpd = SocketServer.TCPServer(("", PORT), Handler)
    print "serving at port", PORT
    httpd.serve_forever()


if __name__ == '__main__':
    main()
